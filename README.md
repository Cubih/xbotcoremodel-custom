# xbotcoremodel
Copy with small modifications from https://github.com/corlab/xbotcoremodel
Used to have a stable version working with "own" projects and reworked yaml in cmake

## Dependencies

System Dependency: **liburdfdom-dev:amd64** 0.2.10+dfsg-1

https://github.com/corlab/robot_model (indigo-devel) -> [**urdf**, **kdl_parser**]

https://github.com/corlab/urdfdom_headers (indigo-devel)

